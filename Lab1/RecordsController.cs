﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab1
{
    class RecordsController
    {
        List<Record> list = new List<Record>();

        public void ChangeRecord(int id, FieldType field, string newValue)
        {

            Record curRecord = FindByID(id);

            switch (field)
            {
                case FieldType.NAME:
                    curRecord.Name = newValue;
                    break;
                case FieldType.SURNAME:
                    curRecord.Surname = newValue;
                    break;
                case FieldType.PROFFESION:
                    curRecord.Proffesion = newValue;
                    break;
                case FieldType.PHONENUMBER:
                    curRecord.PhoneNumber = long.Parse(newValue);
                    break;
                case FieldType.NOTES:
                    curRecord.OtherNotes = newValue;
                    break;
                case FieldType.LASTNAME:
                    curRecord.LastName = newValue;
                    break;
                case FieldType.COUNTRY:
                    curRecord.Country = newValue;
                    break;
                case FieldType.BIRTHDATE:
                    DateTime date = DateTime.Parse(newValue);
                    curRecord.BirthDate = date;
                    break;
                case FieldType.COMPANY:
                    curRecord.Organisation = newValue;
                    break;
            }
        }

        public Record FindByID(int id)
        {
            for (int i = 0; i < list.Count; i++)
                if (list[i].id == id) return list[i];

            return null;
        }


        public void DeleteRecord(int id)
        {
            for (int i = 0; i < list.Count; i++)
                if (list[i].id == id) list.RemoveAt(i);
        }

        public void ShowAllRecords()
        {
            for (int i = 0; i < list.Count; i++) Console.WriteLine(list[i]);
        }



        public void AddRecord(Record record)
        {
            list.Add(record);
        }

        public void ShowRecord(int id)
        {
            for (int i = 0; i < list.Count; i++)
                if (list[i].id == id)
                    Console.WriteLine(list[i].getFullDescription());
        }

    }
}
