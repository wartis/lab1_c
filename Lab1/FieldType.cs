﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab1
{
    public enum FieldType
    {
        SURNAME = 1,
        NAME,
        LASTNAME,
        PHONENUMBER,
        COUNTRY,
        BIRTHDATE,
        COMPANY,
        PROFFESION,
        NOTES
    }

    public static class FieldTypeExtension
    {
        public static int MAX_VALUE = 9;
        public static string GetDescription(FieldType fieldType)
        {
            switch (fieldType)
            {
                case FieldType.SURNAME: return "фамилия";
                case FieldType.NAME: return "имя";
                case FieldType.LASTNAME: return "отчество";
                case FieldType.PHONENUMBER: return "номер телефона (11 цифр)";
                case FieldType.COUNTRY: return "страна";
                case FieldType.BIRTHDATE: return "дата рождения (в формате dd.mm.yyyy)";
                case FieldType.COMPANY: return "компания";
                case FieldType.PROFFESION: return "должность";
                case FieldType.NOTES: return "прочие заметки";
            }
            return null;

        }

        public static bool CheckCorrectness(FieldType fieldType, string value)
        {
            switch (fieldType)
            {
                case FieldType.SURNAME:
                case FieldType.NAME:
                case FieldType.LASTNAME:
                case FieldType.COUNTRY:
                case FieldType.PROFFESION:
                case FieldType.NOTES:
                case FieldType.COMPANY:
                    if (!string.IsNullOrWhiteSpace(value))
                        return true;
                    return false;
                case FieldType.PHONENUMBER:
                    long number;
                    if (long.TryParse(value, out number))
                    {
                        if (value.Length != 11) return false;
                        return true;
                    }

                    return false;
                case FieldType.BIRTHDATE:
                    if (!string.IsNullOrWhiteSpace(value))
                    {
                        string[] dayMonthYear = value.Split(".");
                        int day;
                        int month;
                        int year;
                        if (int.TryParse(dayMonthYear[0], out day) &&
                            int.TryParse(dayMonthYear[1], out month) &&
                            int.TryParse(dayMonthYear[2], out year)
                            )
                        {
                            //1850 - просто число из головы 
                            if (day >= 1 && day <= 31 &&
                                month >= 1 && month <= 12 &&
                                year >= 1850 && year <= 3000
                                ) return true;
                        }
                    }
                    return false;
            }

            return false;
        }

        public static HashSet<FieldType> GetFieldTypeSetByString(string str, params FieldType[] basicValues)
        {
            HashSet<FieldType> uniqValues = new HashSet<FieldType>();
            string[] items = str.Trim().Split(' ');
            for (int i = 0; i < basicValues.Length; i++) uniqValues.Add(basicValues[i]);

            if (string.IsNullOrWhiteSpace(items[0])) return uniqValues;


            for (int i = 0; i < items.Length; i++)
            {
                int curItem = 0;
                if (int.TryParse(items[i], out curItem) && curItem > 0 && curItem <= FieldTypeExtension.MAX_VALUE)
                    uniqValues.Add((FieldType)curItem);
                else return null;
            }

            return uniqValues;

        }
    }
}
