﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab1
{
    class Record
    {
        private static int counter { get; set; }

        public int id;
        public string Surname { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Country { get; set; }
        public string Organisation { get; set; }

        public long PhoneNumber { get; set; }
        public string Proffesion { get; set; }
        public string OtherNotes { get; set; }
        public DateTime BirthDate { get; set; }

        public Record(int id, string surname, string name)
        {
            this.id = id;
            Surname = surname;
            Name = name;
        }

        public Record()
        {
            counter++;
            id = counter;
        }

        public override string ToString()
        {
            string pn = PhoneNumber == 0 ? " " : PhoneNumber.ToString();

            string description = $"ИМЯ: {Name},\n" +
                                 $"ФАМИЛИЯ: {Surname}\n" +
                                 $"НОМЕР ТЕЛЕФОНА: {pn}\n" +
                                 $"ID: {id}\n";



            return description;
        }

        public string getFullDescription()
        {
            string pn = PhoneNumber == 0 ? " " : PhoneNumber.ToString();
            string bd = BirthDate.ToString("dd.MM.yyyy") == "01.01.0001" ? "" : BirthDate.ToString("dd.MM.yyyy");

            string description = $"ИМЯ: {Name},\n" +
                                  $"ФАМИЛИЯ: {Surname}\n" +
                                  $"ОТЧЕСТВО: {LastName}\n" +
                                  $"НОМЕР ТЕЛЕФОНА: {pn}\n" +
                                  $"Дата рождения: {bd}\n" +
                                  $"СТРАНА: {Country}\n" +
                                  $"ОРГАНИЗАЦИЯ: {Organisation}\n" +
                                  $"ДОЛЖНОСТЬ: {Proffesion}\n" +
                                  $"ДРУГИЕ ЗАМЕТКИ: {OtherNotes}\n" +
                                  $"ID: {id}\n";
            return description;

        }
    }
}
