﻿using System;
using System.Collections.Generic;

namespace Lab1
{
    class Program
    {
        private static RecordsController controller = new RecordsController();

        static void Main(string[] args)
        {
            new Program().Start();
        }

        public void Start()
        {
            while (true)
            {
                int id;
                int command;

                Console.Clear();
                Console.WriteLine("Консольная записная телефонная книжка. Доступны следующие команды: ");
                Console.WriteLine("1 - показать всех\n" +
                                  "2 - показать конкретного\n" +
                                  "3 - удалить запись\n" +
                                  "4 - изменить запись\n" +
                                  "5 - добавить запись\n" +
                                  "6 - выйти\n");

                command = InputInteger(1, 6);

                switch (command)
                {
                    case 1:
                        Console.Clear();
                        controller.ShowAllRecords();
                        Console.WriteLine("Введите любую клавишу, чтобы продолжить");
                        Console.ReadKey();
                        break;
                    case 2:
                        Console.Clear();
                        Console.WriteLine("Введите id пользователя, которого вы хотите показать");
                        id = InputInteger(1, int.MaxValue);
                        if (controller.FindByID(id) == null) Console.WriteLine("Пользователя с таким id не существует!");
                        else controller.ShowRecord(id);
                        Console.WriteLine("Введите любую клавишу, чтобы продолжить");
                        Console.ReadKey();
                        break;
                    case 3:
                        Console.Clear();
                        Console.WriteLine("Введите id пользователя, которого вы хотите удалить");
                        id = InputInteger(1, int.MaxValue);
                        if (controller.FindByID(id) == null) Console.WriteLine("Пользователя с таким id не существует!");
                        else controller.DeleteRecord(id);
                        Console.WriteLine("Введите любую клавишу, чтобы продолжить");
                        Console.ReadKey();
                        break;
                    case 4:
                        ChangingMenu();
                        break;
                    case 5:
                        AddingMenu();
                        break;
                    case 6:
                        return;

                }
            }
        }

        //min - inclusive 
        //max - inclusive 
        private static int InputInteger(int min, int max)
        {
            do
            {
                Console.Write("Ну, что решили? ");
                string str = Console.ReadLine();
                int i;
                if (!int.TryParse(str, out i))
                {
                    Console.WriteLine("Как-то грустненько вы числа вводите.\nДавайте ещё разок");
                    continue;
                }
                else
                {
                    if (i >= min && i <= max) return i;
                    Console.WriteLine("Как-то грустненько вы числа вводите.\nДавайте ещё разок");
                    continue;
                }
            } while (true);
        }
        private static void ChangingMenu()
        {
            int id;
            Record curRecord;
            string items; //пункты, которые нужно изменить
            HashSet<FieldType> fields;

            Console.Clear();
            Console.WriteLine("Вас приветствует меню изменения пользователя пользователя. ");
            Console.WriteLine("Введите id записи, которую нужно изменить");
            Console.Write("id: ");
            id = InputInteger(1, int.MaxValue);
            curRecord = controller.FindByID(id);

            if (curRecord == null)
            {
                Console.WriteLine("К сожалению записи с таким id не существует :((\nНажмите любую клавишу, чтобы продолжить");
                Console.ReadKey();
                return;
            }

            Console.WriteLine("Сейчас запись выглядит вот так: \n");
            controller.ShowRecord(id);

            Console.WriteLine("Выберите те пункты, которые необходимо изменить в исходной записи (для этого перечислите их в одну строку через пробел): ");
            for (int i = 1; i <= FieldTypeExtension.MAX_VALUE; i++)
                Console.WriteLine("{0}. {1}", i, FieldTypeExtension.GetDescription((FieldType)i));

            Console.Write("Пункты: ");
            items = Console.ReadLine();
            fields = FieldTypeExtension.GetFieldTypeSetByString(items);
            if (fields != null)
            {
                Console.Clear();
                Console.WriteLine("Заполните следующие поля (ввод будет продолжаться до тех пор, пока не будут введены коректные значения)");

                foreach (FieldType ftype in fields)
                {
                    string inputLine = null;
                    while (!FieldTypeExtension.CheckCorrectness(ftype, inputLine))
                    {
                        Console.Write("{0}: ", FieldTypeExtension.GetDescription(ftype));
                        inputLine = Console.ReadLine();
                    }
                    controller.ChangeRecord(id, ftype, inputLine);
                }

                Console.WriteLine("Запись была успешно изменена.\nНажмите любую клавишу, чтобы продолжить");
                Console.ReadKey();
            }
            else
            {
                Console.WriteLine("Что-то было введено неверно :(");
                Console.WriteLine("Что будем делать дальше?");
                Console.WriteLine("1. Я готов ввести все правильно! Дай мне ещё шанс.");
                Console.WriteLine("2. Назад");
                int chose = InputInteger(1, 2);
                if (chose == 1) ChangingMenu();
                if (chose == 2) return;
            }

        }

        private static void AddingMenu()
        {
            string items; //пункты, которые нужно будет добавлять
            HashSet<FieldType> fields;

            Console.Clear();
            Console.WriteLine("Вас приветствует меню добавления нового пользователя. ");
            Console.WriteLine("Выберите те пункты, которые необходимо ввести (для этого перечислите их в одну строку через пробел): ");
            for (int i = 1; i <= FieldTypeExtension.MAX_VALUE; i++)
                Console.WriteLine("{0}. {1}", i, FieldTypeExtension.GetDescription((FieldType)i));

            Console.WriteLine("!Пункты 1, 2 и 5 являются обязательными!");
            Console.WriteLine("!Если вы не хотите оставлять дополнительную информацию, нажмите enter!");
            Console.Write("1 2 5 ");
            items = Console.ReadLine();
            fields = FieldTypeExtension.GetFieldTypeSetByString(items, new FieldType[3] { FieldType.NAME, FieldType.SURNAME, FieldType.COUNTRY });
            if (fields != null)
            {
                Record record = new Record();
                int recordId = record.id;

                controller.AddRecord(record);
                Console.Clear();
                Console.WriteLine("Заполните следующие поля (ввод будет продолжаться до тех пор, пока не будут введены коректные значения)");
                foreach (FieldType ftype in fields)
                {
                    string inputLine = null;
                    while (!FieldTypeExtension.CheckCorrectness(ftype, inputLine))
                    {
                        Console.Write("{0}: ", FieldTypeExtension.GetDescription(ftype));
                        inputLine = Console.ReadLine();
                    }
                    controller.ChangeRecord(recordId, ftype, inputLine);
                }

                Console.WriteLine("Запись была успешно добавлена.\nНажмите любую клавишу, чтобы продолжить");
                Console.ReadKey();
                Console.Clear();
            }
            else
            {
                Console.WriteLine("Что-то было введено неверно :(");
                Console.WriteLine("Что будем делать дальше?");
                Console.WriteLine("1. Я готов ввести все правильно! Дай мне ещё шанс.");
                Console.WriteLine("2. Назад");
                int chose = InputInteger(1, 2);
                if (chose == 1) AddingMenu();
                if (chose == 2) return;

            }
        }
    }
}
